import Axios from "axios"
import runtimeEnv from '@mars/heroku-js-runtime-env'

const url = runtimeEnv().REACT_APP_API_URL

const fetchQuestions = async (page = null) => {
	if (page) {
		return Axios.get(url + `/api/v1/questions?page=${page}`)
	} else {
		return Axios.get(url + '/api/v1/questions')
	}
}

const fetchRoles = async () => {
	return Axios.get(url + '/api/v1/questions/roles')
}

const fetchTeamingStages = async () => {
	return Axios.get(url + '/api/v1/questions/teaming_stages')
}

const fetchQuestionTypes = async () => {
	return Axios.get(url + '/api/v1/questions/question_types')
}
const fetchConditions = async () => {
	return Axios.get(url + '/api/v1/questions/conditions')
}
const fetchMappings = async () => {
	return Axios.get(url + '/api/v1/questions/mappings')
}

const handleQuestionFormSubmit = values => {
	return Axios.post(url + '/api/v1/questions', { question: values })
}
const handleUpdateFormSubmit = (id, values) => {
	return Axios.put(url + `/api/v1/questions/${id}`, { question: values })
}
const handleQuestionDelete = (id) => {
	return Axios.delete(url + `/api/v1/questions/${id}`);
}

export { fetchQuestions, fetchRoles, fetchTeamingStages, fetchQuestionTypes, fetchConditions, handleQuestionDelete, fetchMappings, handleQuestionFormSubmit, handleUpdateFormSubmit }