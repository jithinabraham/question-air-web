import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Question from '../question'

export default function Router() {
  return (
    <Switch>
      <Route exact path="/" component={Question} />
    </Switch>
  )
}