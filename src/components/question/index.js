import React, { useState, useEffect } from 'react'
import { Row, Col, Button, Card, message, Pagination } from 'antd'
import QuestionForm from './QuestionForm'
import { fetchQuestions, handleQuestionDelete } from '../actions/question/QuestionActions'
import { toCamel } from '../utils'

const Question = () => {
  const [questionForm, setQuestionForm] = useState(false)
  const [questions, setQuestions] = useState([])
  const [currentQuestion, setCurrentQuestion] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [totalPages, setTotalPages] = useState(0)

  const fetchQuestionsList = async (page = null) => {
    try {
      const response = await fetchQuestions(page)
      setQuestions(response.data.questions)
      setTotalPages(response.data.total_count)
      setCurrentPage(response.data.page)
    } catch (e) {
      message.error("Something went wrong, Please try again");
      setQuestions([])
    }
  }


  useEffect(() => {
    fetchQuestionsList()
  }, [])

  const handleAfterFormSubmit = async () => {
    handleClose()
    await fetchQuestionsList()
  }
  const handleClose = () => {
    setQuestionForm(false)
    setCurrentQuestion({})
  }

  const deleteQuestion = async (question) => {
    try {
      await handleQuestionDelete(question.id)
      message.success(`Question "${question.question}" deleted successfully`)
      await fetchQuestionsList()
    } catch (e) {
      message.error("Something went wrong, Please try again")
    }
  }

  const fetchPerPageQuestions = (page, pageSize) => {
    fetchQuestionsList(page)
  }

  const QuestionCard = (props) => {
    const question = props.quesion
    return (
      <Card
        style={{ marginTop: 16 }}
        type="inner"
        title={question.question}
        extra={
          <>
            <Button type="primary" onClick={() => { setQuestionForm(true); setCurrentQuestion(question) }}>
              Edit
        </Button>
            <Button style={{ "marginLeft": "10px" }} type="danger" onClick={() => { deleteQuestion(question) }}>
              Delete
        </Button>
          </>
        }
      >
        <Col lg={8} sm={12} className="fieldpair mb30">
          <div className="fieldpair__label mb10">
            <b>Appear</b>
          </div>
          <div className="fieldpair__value">{question.appear}</div>
        </Col>
        <Col lg={8} sm={12} className="fieldpair mb30">
          <div className="fieldpair__label mb10">
            <b>Priority</b>
          </div>
          <div className="fieldpair__value">{question.priority}</div>
        </Col>
        <Col lg={8} sm={12} className="fieldpair mb30">
          <div className="fieldpair__label mb10">
            <b>Required</b>
          </div>
          <div className="fieldpair__value">{question.is_required ? "Yes" : "No"}</div>
        </Col>
        <Col lg={8} sm={12} className="fieldpair mb30">
          <div className="fieldpair__label mb10">
            <b>Frequency</b>
          </div>
          <div className="fieldpair__value">{question.frequency}</div>
        </Col>
        <Col lg={8} sm={12} className="fieldpair mb30">
          <div className="fieldpair__label mb10">
            <b>Teaming Stage</b>
          </div>
          <div className="fieldpair__value">{toCamel(question.teaming_stage)}</div>
        </Col>
        <Col lg={8} sm={12} className="fieldpair mb30">
          <div className="fieldpair__label mb10">
            <b>Type</b>
          </div>
          <div className="fieldpair__value">{toCamel(question.question_type)}</div>
        </Col>
        <Col lg={8} sm={12} className="fieldpair mb30">
          <div className="fieldpair__label mb10">
            <b>Condition</b>
          </div>
          <div className="fieldpair__value">{toCamel(question.condition)}</div>
        </Col>
        <Col lg={8} sm={12} className="fieldpair mb30">
          <div className="fieldpair__label mb10">
            <b>Role</b>
          </div>
          <div className="fieldpair__value">{toCamel(question.role)}</div>
        </Col>
        <Col lg={8} sm={12} className="fieldpair mb30">
          <div className="fieldpair__label mb10">
            <b>Mapping</b>
          </div>
          <div className="fieldpair__value">{toCamel(question.mapping)}</div>
        </Col>
      </Card>
    )
  }

  return (
    <div className="questions">
      <Row>
        <Col span={4}>
          <h2> Questions</h2>
        </Col>
        <Col span={2} className="questions__create">
          <Button type="primary" onClick={() => { setCurrentQuestion({}); setQuestionForm(true); }}>
            Create Question
          </Button>
        </Col>
      </Row>
      <Row>
        <Pagination defaultCurrent={currentPage} total={totalPages} onChange={fetchPerPageQuestions} />
      </Row>
      <Row>
        {questions.map((question) => <QuestionCard quesion={question} key={question.id} />)}
      </Row>
      <QuestionForm
        showForm={questionForm}
        handleAfterFormSubmit={handleAfterFormSubmit}
        currentQuestion={currentQuestion}
        handleClose={handleClose}
      />
    </div>
  )
}

export default Question;