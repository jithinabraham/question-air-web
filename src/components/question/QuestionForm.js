import React from 'react'
import { Modal } from 'antd';
import WrappedQuestionForm from './QuestionFormTemplate';

const QuestionForm = props => {
	const handleCancel = () => {
		console.log('Clicked cancel button');
		props.handleClose()
	};

	return (
		<div>
			{props.showForm && <Modal
				title="New Question"
				visible={props.showForm}
				onCancel={handleCancel}
				footer={null}
			>
				<WrappedQuestionForm
					handleAfterFormSubmit={props.handleAfterFormSubmit}
					currentQuestion={props.currentQuestion} />
			</Modal>}
		</div>
	);
}

export default QuestionForm;