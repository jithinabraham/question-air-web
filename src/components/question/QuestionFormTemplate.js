import React, { useState, useEffect } from 'react'
import { Form, Input, Button, Select, Radio, InputNumber, message } from 'antd';
import { fetchRoles, fetchQuestionTypes, fetchTeamingStages, fetchConditions, fetchMappings, handleQuestionFormSubmit, handleUpdateFormSubmit } from '../actions/question/QuestionActions';
import { toCamel } from '../utils';
const { TextArea } = Input;
const { Option } = Select;

const QuestionFormTemplate = props => {
	const [roles, setRoles] = useState([])
	const [questionTypes, setQuestionTypes] = useState([])
	const [teamingStages, setTeamingStages] = useState([])
	const [conditions, setConditions] = useState([])
	const [mappings, setMappings] = useState([])

	const fetchSelectFields = async () => {
		try {
			const rolesRes = await fetchRoles();
			setRoles(rolesRes.data)
			const questionTypesRes = await fetchQuestionTypes();
			setQuestionTypes(questionTypesRes.data.question_types)
			const teamingStagesRes = await fetchTeamingStages()
			setTeamingStages(teamingStagesRes.data.teaming_stages)
			const conditionsRes = await fetchConditions();
			setConditions(conditionsRes.data.conditions)
			const mappingsRes = await fetchMappings();
			setMappings(mappingsRes.data)
		} catch (e) {
			setRoles([])
			setQuestionTypes([])
			setTeamingStages([])
			setConditions([])
			setMappings([])
		}
	}

	useEffect(() => {
		fetchSelectFields()
	}, [])
	const formItemLayout = {
		labelCol: {
			xs: { span: 24 },
			sm: { span: 8 },
		},
		wrapperCol: {
			xs: { span: 24 },
			sm: { span: 16 },
		},
	};

	const tailFormItemLayout = {
		wrapperCol: {
			xs: {
				span: 24,
				offset: 0,
			},
			sm: {
				span: 16,
				offset: 8,
			},
		},
	};

	const handleSubmit = e => {
		e.preventDefault();
		props.form.validateFieldsAndScroll(async (err, values) => {
			if (!err) {
				try {
					if (props.currentQuestion.id) {
						await handleUpdateFormSubmit(props.currentQuestion.id, values)
						message.success("Question created successfully!")
					} else {
						await handleQuestionFormSubmit(values)
						message.success("Question updated successfully!")
					}
					props.handleAfterFormSubmit(false)
				} catch (error) {
					console.log(error)
					message.error("There is a problem on submitting the form, Please try again")
				}
			}
		});
	};

	const { getFieldDecorator } = props.form;

	return (
		<Form {...formItemLayout} onSubmit={handleSubmit}>
			<Form.Item label="Question">
				{getFieldDecorator('question', {
					rules: [
						{
							required: true,
							message: 'Please input your Question!',
						},
					],
					initialValue: props.currentQuestion.question
				})(<TextArea rows={4} />)}
			</Form.Item>
			<Form.Item label="Required">
				{getFieldDecorator('is_required', {
					rules: [
						{
							required: true,
							message: 'Please select an option!',
						},
					],
					initialValue: props.currentQuestion.is_required
				})(<Radio.Group>
					<Radio value={true}>Yes</Radio>
					<Radio value={false}>No</Radio>
				</Radio.Group>)}
			</Form.Item>
			<Form.Item label="Appear">
				{getFieldDecorator('appear', {
					rules: [
						{
							required: true,
							message: 'Please input Appear',
						},
					],
					initialValue: props.currentQuestion.appear
				})(<InputNumber />)}
			</Form.Item>
			<Form.Item label="Frequency">
				{getFieldDecorator('frequency', {
					rules: [
						{
							required: true,
							message: 'Please input Frequency',
						},
					],
					initialValue: props.currentQuestion.frequency
				})(<InputNumber />)}
			</Form.Item>
			<Form.Item label="Priority">
				{getFieldDecorator('priority', {
					rules: [
						{
							required: true,
							message: 'Please input Priority',
						},
					],
					initialValue: props.currentQuestion.priority
				})(<InputNumber />)}
			</Form.Item>
			<Form.Item label="Role">
				{getFieldDecorator('role_id', {
					rules: [
						{
							required: true,
							message: 'Please select Role',
						},
					],
					initialValue: props.currentQuestion.role_id
				})(<Select
					showSearch
					style={{ width: 200 }}
					placeholder="Select a role"
					optionFilterProp="children"
					filterOption={(input, option) =>
						option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
					}
				>
					{roles.map((role) => <Option value={role.id} key={role.id}>{role.name}</Option>)}
				</Select>)}
			</Form.Item>
			<Form.Item label="Condtion">
				{getFieldDecorator('condition', {
					rules: [
						{
							required: true,
							message: 'Please select Condition',
						},
					],
					initialValue: props.currentQuestion.condition
				})(<Select
					showSearch
					style={{ width: 200 }}
					placeholder="Select a condition"
					optionFilterProp="children"
					filterOption={(input, option) =>
						option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
					}
				>
					{conditions.map((condition) => <Option value={condition} key={condition}>{toCamel(condition)}</Option>)}
				</Select>)}
			</Form.Item>
			<Form.Item label="Mapping">
				{getFieldDecorator('mapping_id', {
					rules: [
						{
							required: true,
							message: 'Please select Mapping',
						},
					],
					initialValue: props.currentQuestion.mapping_id
				})(<Select
					showSearch
					style={{ width: 200 }}
					placeholder="Select a mapping"
					optionFilterProp="children"
					filterOption={(input, option) =>
						option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
					}
				>
					{mappings.map((mapping) => <Option value={mapping.id} key={mapping.id}>{mapping.name}</Option>)}
				</Select>)}
			</Form.Item>
			<Form.Item label="Question Type">
				{getFieldDecorator('question_type', {
					rules: [
						{
							required: true,
							message: 'Please select Question type',
						},
					],
					initialValue: props.currentQuestion.question_type
				})(<Select
					showSearch
					style={{ width: 200 }}
					placeholder="Select a Question type"
					optionFilterProp="children"
					filterOption={(input, option) =>
						option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
					}
				>
					{questionTypes.map((questionType) => <Option value={questionType} key={questionType}>{toCamel(questionType)}</Option>)}
				</Select>)}
			</Form.Item>
			<Form.Item label="Teaming stage">
				{getFieldDecorator('teaming_stage', {
					rules: [
						{
							required: true,
							message: 'Please select Teaming stage',
						},
					],
					initialValue: props.currentQuestion.teaming_stage
				})(<Select
					showSearch
					style={{ width: 200 }}
					placeholder="Select a Teaming stage"
					optionFilterProp="children"
					filterOption={(input, option) =>
						option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
					}
				>
					{teamingStages.map((teamingStage) => <Option value={teamingStage} key={teamingStage}>{toCamel(teamingStage)}</Option>)}
				</Select>)}
			</Form.Item>
			<Form.Item {...tailFormItemLayout} >
				<Button type="primary" htmlType="submit">
					Submit
          </Button>
			</Form.Item>
		</Form>
	)
}

const WrappedQuestionForm = Form.create({ name: 'questionForm' })(QuestionFormTemplate);
export default WrappedQuestionForm;
