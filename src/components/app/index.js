import React from "react";
import Router from "../router/index";
import { Layout, Menu } from 'antd';
import 'antd/dist/antd.css';
import './main.css'
const { Header, Content, Footer } = Layout;


const App = () => {
  return (
    <div id='question-app'>
      <Layout className="layout">
        <Header>
          <div className="logo"></div>
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={['2']}
            style={{ lineHeight: '64px' }}
          >
          </Menu>
        </Header>
        <Content style={{ padding: '0 50px' }}>
          <div style={{ background: '#fff', padding: 24, minHeight: 500 }}><Router /></div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>Dumy App ©2020 Created by Jithin</Footer>
      </Layout>
    </div>)
};

export default App;