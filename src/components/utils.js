const toCamel = (s) => {
	return s.charAt(0).toUpperCase() + s.slice(1).replace('_', " ")
};

export { toCamel }